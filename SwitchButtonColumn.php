<?php

namespace morningbird\grid;

class SwitchButtonColumn extends \yii\grid\DataColumn {
    public $inputClass = '';
    public function init() {
        \morningbird\assets\BootstrapToggleAsset::register(\Yii::$app->controller->getView());
        parent::init();
    }

    /**
     * 
     * @param \yii\db\ActiveRecord $model
     * @param type $key
     * @param type $index
     * @return type
     */
    protected function renderDataCellContent($model, $key, $index) {
        $id = 'inputToggle' . uniqid(rand(0, 10000));
        $curView = \Yii::$app->controller->getView();
        $curView->registerJs("
            $('#{$id}').bootstrapToggle({
                on: 'Ya',
                off: 'Tidak'
            });
        ");
            
        $options = [
            'id' => $id,
            'class' => $this->inputClass,
            'label' => false
        ];
        
        if(is_array($model->getPrimaryKey()))
        {
            foreach($model->getPrimaryKey() as $key => $value)
            {
              $options['data-' . $key] = $value;
            }
        }
        else {
            foreach($model->getPrimaryKey(true) as $key => $value)
            {
              $options['data-' . $key] = $value;
            }
        }
        
        return \morningbird\helpers\Html::activeCheckbox($model, $this->attribute, $options);
    }
}
