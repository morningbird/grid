<?php

namespace morningbird\grid;

class LinkColumn extends \yii\grid\DataColumn {

    public $url;

    protected function renderDataCellContent($model, $key, $index) {
        $attr = $this->attribute;
        $url = $this->url;
        //ambil semua attribute kalau ada
        $matches = [];
        preg_match_all('/\{([a-zA-Z_]+)\}/', $url, $matches);
//        return $model->sku;
        if(isset($matches[1]))
        {
          for($i=0;$i<count($matches[0]);$i++)
          {
            $temp1 = $matches[1][$i];
            $needle = $matches[0][$i];
            $value = $model->$temp1;
            
            $url = str_replace($needle, $value, $url);
          }
        }
        return \morningbird\helpers\Html::a($model->$attr, $url);
    }
}
