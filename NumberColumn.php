<?php

namespace morningbird\grid;

class NumberColumn extends \yii\grid\DataColumn {
    public $inputClass = '';
    protected function renderDataCellContent($model, $key, $index) {
        $attr = $this->attribute;
        $pk = key($model->getPrimaryKey());
        $options = [
            'class' => 'form-control ' . $this->inputClass,
            'type' => 'number',
            "data-{$pk}" => $model->$pk
        ];
        return \morningbird\helpers\Html::activeTextInput($model, $attr, $options);
    }
}
