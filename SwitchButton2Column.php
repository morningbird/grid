<?php

namespace morningbird\grid;

use yii\helpers\Html;

class SwitchButton2Column extends \yii\grid\DataColumn {
    public $inputClass = '';
    public $ajaxAction = false;
    public $ajaxUrl;
    public $ajaxConfirmMessage;
    public function init() {
        \morningbird\assets\BootstrapSwitchAsset::register(\Yii::$app->controller->getView());
        parent::init();
    }

    protected function setUpAjax($elID, $params)
    {
        $curView = \Yii::$app->controller->getView();
        \dominus77\sweetalert2\assets\SweetAlert2Asset::register($curView);
        $useSweetAlert = false;
        $paramString = json_encode($params);
        if(!empty($this->ajaxConfirmMessage))
        {
            $useSweetAlert = true;
            $jsAjax = "swal({
                    title: '{$this->ajaxConfirmMessage}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal',
                    showLoaderOnConfirm: true,
                    preConfirm: function (email) {
                        return new Promise(function (resolve, reject) {
                            $.get('{$this->ajaxUrl}', params, function(){
                                resolve();
                            });
                        })
                    },
                    allowOutsideClick: false
                }).then(function(){
                    
                }, function (dismiss) {
                    
                });";
        }
        else {
            
            $jsAjax = "
                swal({
                    allowOutsideClick: false,
                    title: 'Memproses permintaan Anda',
                    onOpen: function () {
                        swal.showLoading();
                        $.get('{$this->ajaxUrl}', params, function(){
                            that.bootstrapSwitch('toggleState', true, true);
                            swal.close();
                        }).fail(function() {
                            swal.close();
                            swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                            );
                        });;
                    }
                  }).then(
                    function () {},
                    // handling the promise rejection
                    function (dismiss) {
                    }
                )
            ";
        }
        $curView->registerJs("
            $('#{$elID}').on('switchChange.bootstrapSwitch', function (e, data) {
                var that = $(this);
                that.bootstrapSwitch('state', !data, true);
                var params = {$paramString};
                params.val = $(this).prop('checked');
                {$jsAjax};
            });
        ");
    }
    
    /**
     * 
     * @param \yii\db\ActiveRecord $model
     * @param type $key
     * @param type $index
     * @return type
     */
    protected function renderDataCellContent($model, $key, $index) {
        $id = 'inputToggle' . uniqid(rand(0, 10000));
        $curView = \Yii::$app->controller->getView();
        $curView->registerJs("
            $('#{$id}').bootstrapSwitch({
                onText: 'Ya',
                offText: 'Tidak'
            });
        ");
            
        $options = [
            'id' => $id,
            'class' => $this->inputClass,
            'label' => false
        ];
        
        //param untuk ajax
        $ajaxParams = [];
        if(is_array($model->getPrimaryKey()))
        {
            foreach($model->getPrimaryKey() as $key => $value)
            {
              $options['data-' . $key] = $value;
              $ajaxParams[$key] = $value;
            }
        }
        else {
            foreach($model->getPrimaryKey(true) as $key => $value)
            {
              $options['data-' . $key] = $value;
              $ajaxParams[$key] = $value;
            }
        }
        
        //jika ada request ajax
        if($this->ajaxAction)
        {
            $this->setUpAjax($id, $ajaxParams);
        }

        return Html::activeCheckbox($model, $this->attribute, $options);
    }
}
